param(
    [Parameter(Mandatory)]
    [ValidatePattern(".+")]
    [String]$sourceDir,
    [Parameter(Mandatory)]
    [ValidatePattern(".+")]
    [String]$destinationDir,
    [Parameter(Mandatory)]
    [ValidatePattern("\\\\.+")]
    [String]$remoteDir,
    [String]$eventlogExportScriptPath = ".\eventlogExport.vbs"
)

$ErrorActionPreference = "Stop"

function Save-In-Local([String]$sourceDir, [String]$destinationHourDir) {
    mkdir $destinationHourDir -Force
    Xcopy $sourceDir\* $destinationHourDir /d:$yesterday /y
}

Set-Variable yesterday -Value ([DateTime]::Now.AddDays(-1).ToString('MM-dd-yyyy'))
Set-Variable ymdh -Value ([DateTime]::Now.ToString('yyyyMMdd_HH'))
Set-Variable destinationHourDir -Value $destinationDir\$ymdh

Save-In-Local $sourceDir $destinationHourDir
cscript $eventlogExportScriptPath //NOLOGO >$destinationHourDir\eventlog.csv
Copy-Item $destinationHourDir $remoteDir -Recurse -Force

return

<#
* https://docs.microsoft.com/ja-jp/powershell/scripting/developer/cmdlet/validatepattern-attribute-declaration?view=powershell-7.2
* http://fsystem-web.sakura.ne.jp/wp/?page_id=95
* https://docs.microsoft.com/ja-jp/windows-server/administration/windows-commands/xcopy
* https://greptips.com/posts/801/
* http://www.vwnet.jp/windows/PowerShell/2018061001/EditDateTime.htm
* https://docs.microsoft.com/ja-jp/powershell/module/microsoft.powershell.core/about/about_return?view=powershell-5.1&source=docs
* https://docs.microsoft.com/ja-jp/powershell/scripting/developer/help/examples-of-comment-based-help?view=powershell-5.1
* https://www.ipswitch.com/jp/blog/use-powershell-copy-item-cmdlet-transfer-files-winrm
* https://docs.microsoft.com/ja-jp/powershell/scripting/samples/working-with-files-and-folders?view=powershell-7.2
* https://www.startpassion.life/entry/2020/12/29/220927
* https://docs.microsoft.com/ja-jp/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.2
#>
